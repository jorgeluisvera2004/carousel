# Carousel in React With Redux

The carousel is an example of managing requests to a server.

## Features

- Redux / Async
- Redux Toolkit
- Lazy Loading
- CSS Flex
- CSS Grid
- Custom Modal
- Responsive design
- Unit Tests (With events)
- Unit Test on Redux

## Run Server

```bash
cd sever
node app.js
```

## Installation And Run

Use the package manager [npm](https://www.npmjs.com/) to install the project.

```bash
cd client
npm install 
npm start
```

## Run Unit Test

Note: Run from the client folder

```bash
npm test
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)