const slides = [
  {id: 1, name: 'Sea', uri: 'https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bc01c83c3da0425e9baa6c7a9204af81'},
  {id: 2, name: 'Sea', uri: 'https://techcrunch.com/wp-content/uploads/2017/05/gettyimages-580833893.jpg'},
  {id: 3, name: 'Mediterrean Sea', uri: 'https://www.impel.eu/wp-content/uploads/2019/08/mediterranean-sea.jpg'},
  {id: 4, name: 'Island', uri: 'https://media-cdn.tripadvisor.com/media/photo-s/19/f4/54/37/wilson-island.jpg'},
  {id: 5, name: 'Town Island', uri: 'https://pix10.agoda.net/hotelImages/5750598/-1/a5efdca085d406eb617aa48fd208ea3e.jpg'},
  {id: 6, name: 'Mexico Beach', uri: 'https://pix10.agoda.net/hotelImages/301716/-1/fe9724d8fb4da3dd4590353bd771a276.jpg'},
  {id: 7, name: 'Town', uri: 'https://image.freepik.com/vector-gratis/escena-fondo-muchas-casas-pueblo_1308-10384.jpg'},
  {id: 8, name: 'Town', uri: 'https://image.freepik.com/foto-gratis/hermosa-vista-casco-antiguo-segovia-espana_181624-47354.jpg'},
];

export default slides;