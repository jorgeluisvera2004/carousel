import { Fragment } from 'react';
import ReactDOM from 'react-dom';

import classes from './Modal.module.css';

const Backdrop = (props) => {
  return <div className={classes.backdrop} onClick={props.onClose}/>;
};

const ModalOverlay = (props) => {
  return (
    <div className={classes.modal}>
      <div className={classes.content}>{props.children}</div>
    </div>
  );
};

let debugElement = document.createElement('div'); // Only for testing purposes
let debugElement2 = document.createElement('div');
debugElement2.setAttribute("id", "overlays");
debugElement.append(debugElement2);
let documentBody = document.body;
documentBody.append(debugElement)

const portalElement = document.getElementById('overlays') || documentBody;

const Modal = (props) => {
  return (
    <Fragment>
      {ReactDOM.createPortal(<Backdrop onClose={props.onClose} />, portalElement)}
      {ReactDOM.createPortal(
        <ModalOverlay>{props.children}</ModalOverlay>,
        portalElement
      )}
    </Fragment>
  );
};

export default Modal;