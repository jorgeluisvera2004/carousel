import { render, screen } from '@testing-library/react';
import App from './App';
import store from './store/index';
import { Provider } from 'react-redux';

test('render title', () => {
  render(<Provider store={store}><App /></Provider>);
  const titleElelemnt = screen.getByText('Carousel');
  expect(titleElelemnt).toBeInTheDocument();
});
