//import logo from './logo.svg';
import './App.css';

import Carousel from "./components/carousel/Carousel";

function App() {
  return (
    <div className="main-container">
      <h1 className="main-title">Carousel</h1>
      <Carousel></Carousel>
    </div>
  );
}

export default App;
