import { configureStore } from '@reduxjs/toolkit';

import carouselReducer from './carousel-slice';


const store = configureStore({
  reducer: { carousel: carouselReducer }
});

export default store;
