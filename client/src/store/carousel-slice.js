import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isLoaded: false,
  currentIndex: 0,
  error: '',
  slides:[],
  itemsPerPage:2,
  items:[],
  keyword:'',
};

const carouselSlice = createSlice({
  name: 'carousel',
  initialState: initialState,
  reducers: {
    setIsLoaded(state, action) {
      state.isLoaded = action.payload.error;
    },
    setError(state, action) {
      state.error = action.payload.error;
    },
    setSlides(state, action){
      const windowWidth = window.innerWidth;
      state.itemsPerPage = windowWidth > 1200 ? 4 : windowWidth > 600 ? 2 : 1 ;
      state.isLoaded = true;
      state.error = '';
      state.slides = action.payload.slides;
      state.currentIndex = 0;
      state.items = state.slides.filter((val, i)=> i < state.itemsPerPage);
    },
    setItemsPerPage(state, action){
      state.itemsPerPage = action.payload.itemsPerPage;
      state.currentIndex = 0;
      state.items = state.slides.filter((val, i)=> i < action.payload.itemsPerPage);
    },
    prevPage(state){
      const newIndex = state.currentIndex - state.itemsPerPage;
      if (newIndex >= 0 ) {
        const endIndex = (newIndex + state.itemsPerPage)-1;
        let newItems = state.slides.filter((val, i)=> i >= newIndex && i <= endIndex );
        state.items = newItems;
        state.currentIndex = newIndex;
      }
    },
    nextPage(state){
      const newIndex = (state.currentIndex + state.itemsPerPage);
      if (newIndex <= state.slides.length - 1) {
        const endIndex = (newIndex + state.itemsPerPage)-1;
        let newItems = state.slides.filter((val, i)=> i >= newIndex && i <= endIndex );
        state.items = newItems;
        state.currentIndex = newIndex;
      }
    },
  },
});

export const carouselActions = carouselSlice.actions;

export default carouselSlice.reducer;