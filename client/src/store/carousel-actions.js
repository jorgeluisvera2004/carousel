import { carouselActions } from './carousel-slice';
import { baseUrl } from '../helpers/defaultValues';

export const fetchData = (keyword='') => {
  return async (dispatch) => {

    const fetchData = async () => {
      const response = await fetch(
        `${baseUrl}?term=${keyword}`
      );

      if (!response.ok) {
        throw new Error('Could not fetch the data!');
      }

      const data = await response.json();
      return data;
    };

    try {
      const reponseData = await fetchData();
      dispatch(
        carouselActions.setSlides({
          slides: reponseData || [],
        })
      );
    } catch (error) {
      dispatch(
        carouselActions.setError({
          error: error
        })
      );
    }
    
  };
};