import React, {lazy, Suspense} from 'react'
import clases from './List.module.css'

const Item = lazy(() => import('./Item'));

function List(props) {
  return (
    <div className={`${clases['carousel-content']} ${clases[`carousel-content-${props.perPage}`]} `}>
      { props.items.map((it) => {
        return (<Suspense key={it.id} fallback={<div><i className="fas fa-sync fa-spin"></i> Loading...</div>}>
            <Item info={it} />
          </Suspense>)
      }) }
    </div>
  )
}

export default List
