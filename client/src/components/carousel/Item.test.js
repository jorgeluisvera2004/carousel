import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Item from './Item';
//import Modal from '../../UI/Modal';

describe('Item component', () => {
  test('Render Item', () => {
    const record = {id: 1, name:'Sea', url:'https://techcrunch.com/wp-content/uploads/2017/05/gettyimages-580833893.jpg'};
    render(<Item info={record}></Item>)

    const imageElement = screen.getByRole('img');
    expect(imageElement).toBeInTheDocument();
  })
  
  test('Click on image', () => {
    const record = {id: 1, name:'Sea', url:'https://techcrunch.com/wp-content/uploads/2017/05/gettyimages-580833893.jpg'};
    render(<Item info={record}></Item>)

    const imageElement = screen.getByRole('img');
    userEvent.click(imageElement);

    const closeElement = screen.getByRole('button');
    expect(closeElement).toBeInTheDocument();
  })
  
});