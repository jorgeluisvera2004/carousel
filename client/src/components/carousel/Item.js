import React, {useState} from 'react'
import Modal from '../../UI/Modal';
import clases from './Item.module.css'

function Item(props) {
  const [modalIsShown, setModalIsShown] = useState(false);

  const showModalHandler = () => {
    setModalIsShown(true);
  };

  const hideModalHandler = () => {
    setModalIsShown(false);
  };

  const customModal = (
    <Modal onClose={hideModalHandler}>
      <button className={clases['close-modal']} onClick={hideModalHandler}>
        <i className="far fa-times-circle"></i>
      </button>
      <div className="">
        <img src={props.info.uri} alt={props.info.name} style={{width:"100%"}} />
        <label>{props.info.name}</label>
      </div>
    </Modal>
  );
  
  return (
    <div className={clases['carousel-item']}>
      {modalIsShown && customModal}
      <img src={props.info.uri} alt={props.info.name} onClick={showModalHandler} />
    </div>
  )
}

export default Item
