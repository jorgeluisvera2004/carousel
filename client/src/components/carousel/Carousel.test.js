import { render, fireEvent, screen } from '@testing-library/react';
import { baseUrl } from '../../helpers/defaultValues';
import Carousel from './Carousel';
import store from '../../store/index';
import { Provider } from 'react-redux';

import { rest } from 'msw'
import { setupServer } from 'msw/node'

const server = setupServer(
  rest.get(baseUrl, (req, res, ctx) => {
    const info = [{ id: 1, name: 'Sea', url: 'https://techcrunch.com/wp-content/uploads/2017/05/gettyimages-580833893.jpg' },
      { id: 2, name: 'Town', url: 'https://image.freepik.com/vector-gratis/escena-fondo-muchas-casas-pueblo_1308-10384.jpg' }
    ];
    return res(ctx.json(info), ctx.delay(150))
  }),
);

describe('Carousel async component', () => {
  beforeAll(() => server.listen({ onUnhandledRequest: "bypass" }));
  beforeEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test('renders images if request succeeds', async () => {
    render(<Provider store={store}><Carousel /></Provider>)

    const listItemElements = await screen.findAllByRole('img');
    expect(listItemElements).not.toHaveLength(0);
  })
})