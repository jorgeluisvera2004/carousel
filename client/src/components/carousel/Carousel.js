import React, { Fragment, useEffect } from 'react'
import { useDispatch, useSelector } from "react-redux";

import List from './List'
import { carouselActions } from '../../store/carousel-slice';
import { fetchData } from '../../store/carousel-actions';
import clases from './Carousel.module.css'
import Form from './Form';
import Alert from '../../UI/Alert/Alert';


function Carousel() {
  const dispatch = useDispatch();
  const items = useSelector((state) => state.carousel.items);
  const isLoaded = useSelector((state) => state.carousel.isLoaded);
  const error = useSelector((state) => state.carousel.error);
  const itemsPerPage = useSelector((state) => state.carousel.itemsPerPage);

  useEffect(() => {
    dispatch(fetchData());
  }, [dispatch])

  useEffect(() => {
    function handleResize() {
      const windowWidth = window.innerWidth;
      const itemsPerPage = windowWidth > 1200 ? 4 : windowWidth > 600 ? 2 : 1 ;
      dispatch(carouselActions.setItemsPerPage({itemsPerPage: itemsPerPage}) );
    }
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  const moveHandler = (ev, direction) => {
    ev.preventDefault();
    if( direction==='LEFT' ){
      dispatch(carouselActions.prevPage() );
    }
    if( direction==='RIGHT' ){
      dispatch(carouselActions.nextPage() );
    }
  }

  const onSearchSlidesHandler = (word) => {
    dispatch(carouselActions.setIsLoaded(false) );
    dispatch(fetchData(word));
  }

  let carouselSection = (
    <section className={clases.carousel} >
        <button className={clases.left} onClick={(ev)=>{moveHandler(ev,'LEFT')}}><i className="fas fa-chevron-left"></i></button>
        <List items={items} perPage={itemsPerPage}></List>
        <button className={clases.right} onClick={(ev)=>{moveHandler(ev,'RIGHT')}}><i className="fas fa-chevron-right"></i></button>
      </section>
  );
  if( ! items.length > 0 ){
    carouselSection = (
      <Alert title="Error!" type="error">
        No records found
      </Alert>
    );
  }

  const loader = (<div className={clases.loading}>
    <i className="fas fa-spinner fa-pulse fa-4x"></i>
  </div>);

  return (
    <Fragment>
      <Form onSearchHandler={onSearchSlidesHandler} />
      {!error && !isLoaded && loader}
      {!error && isLoaded && carouselSection}
      {!!error && <Alert title="Error loading data!" type="error">error</Alert>}
    </Fragment>
  )
}

export default Carousel
