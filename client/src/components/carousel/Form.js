import React, { useRef } from 'react'
import clases from './Form.module.css'


export default function Form(props) {
  const refKeyword = useRef('');

  const onSendForm = (ev) => {
    ev.preventDefault();
    props.onSearchHandler(refKeyword.current.value);
  }

  return (
    <form className={clases['form-container']} onSubmit={onSendForm} >
      <input ref={refKeyword} type="text" className={clases.keyword} placeholder="Search..." />
      <button type="submit" className={clases.go}>
        <i className="fas fa-search"></i>
      </button>
    </form>
  )
}
