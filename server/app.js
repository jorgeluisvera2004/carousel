const http = require('http');
const url = require('url');

const hostname = '127.0.0.1';
const port = 3002;

const server = http.createServer((req, res) => {

  const queryObject = url.parse(req.url,true).query;
  //console.log(queryObject);


  const slides = [
    {id: 1, name: 'House Sound', uri: 'https://images.unsplash.com/5/unsplash-kitsune-4.jpg?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bc01c83c3da0425e9baa6c7a9204af81'},
    {id: 2, name: 'Town Island', uri: 'https://pix10.agoda.net/hotelImages/5750598/-1/a5efdca085d406eb617aa48fd208ea3e.jpg'},
    {id: 3, name: 'Mediterrean Sea', uri: 'https://www.impel.eu/wp-content/uploads/2019/08/mediterranean-sea.jpg'},
    {id: 4, name: 'Mexico Beach', uri: 'https://pix10.agoda.net/hotelImages/301716/-1/fe9724d8fb4da3dd4590353bd771a276.jpg'},
    {id: 5, name: 'Island', uri: 'https://media-cdn.tripadvisor.com/media/photo-s/19/f4/54/37/wilson-island.jpg'},
    {id: 6, name: 'Town', uri: 'https://image.freepik.com/vector-gratis/escena-fondo-muchas-casas-pueblo_1308-10384.jpg'},
    {id: 7, name: 'Sea', uri: 'https://techcrunch.com/wp-content/uploads/2017/05/gettyimages-580833893.jpg'},
    {id: 8, name: 'Town', uri: 'https://image.freepik.com/foto-gratis/hermosa-vista-casco-antiguo-segovia-espana_181624-47354.jpg'},
    {id: 9, name: 'Sea', uri: 'https://image.freepik.com/vector-gratis/fondo-mar-videoconferencias_52683-43441.jpg'},
    {id: 10, name: 'River', uri: 'https://image.freepik.com/vector-gratis/escena-naturaleza-rio-colinas-bosque-montana-ilustracion-estilo-dibujos-animados-planos-paisaje_1150-37306.jpg'},
    {id: 11, name: 'Sea', uri: 'https://image.freepik.com/vector-gratis/plantilla-colorida-vida-submarina_1284-37130.jpg'},
    {id: 12, name: 'Beach', uri: 'https://image.freepik.com/foto-gratis/foto-fascinante-paisaje-marino-hermosa-puesta-sol_181624-47241.jpg'},
    {id: 13, name: 'Sea', uri: 'https://image.freepik.com/vector-gratis/acuarela-ballena-jorobada-vector-banner-acuarela_53876-128031.jpg'},
    {id: 14, name: 'Ocean', uri: 'https://image.freepik.com/foto-gratis/oleadas-mar-profundo_185937-62.jpg'},
    {id: 15, name: 'Surfing', uri: 'https://image.freepik.com/foto-gratis/surfista-surfeando-mar-hermosas-olas_181624-166.jpg'},
    {id: 16, name: 'Ocean', uri: 'https://image.freepik.com/foto-gratis/cola-ballena-jorobada-frente-velero_159160-160.jpg'},
    {id: 17, name: 'Sea', uri: 'https://image.freepik.com/foto-gratis/tiro-angulo-alto-oceano-diferentes-tonos-azul-samos-grecia_181624-6862.jpg'},
    {id: 18, name: 'River', uri: 'https://image.freepik.com/vector-gratis/conjunto-elemento-agua-aislado_1308-26027.jpg'},
  ];

  let respnonse = slides;
  if( queryObject.term ){
    const keyword = queryObject.term.toLowerCase();
    respnonse = slides.filter((val)=> val.name.toLowerCase().includes(keyword) > 0 );
  }

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.setHeader("Access-Control-Allow-Origin", "*")
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  res.end(JSON.stringify(respnonse));
});

server.listen(port, hostname, () => {
  console.log(`The server is running on http://${hostname}:${port}/`);
});